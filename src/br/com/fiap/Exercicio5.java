package br.com.fiap;

import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class Exercicio5 {
	public static void main(String[] args) {
		double salario, inss, irpf, salarioNovo;
		float planoSaude = 149;
		DecimalFormat df = new DecimalFormat("###,##0.00");
		
		try {
			salario = Double.parseDouble(JOptionPane.showInputDialog("Digite o seu salário atual"));
			salarioNovo = salario + salario * 0.15;
			JOptionPane.showMessageDialog(null, "Seu novo salário é: " + salarioNovo);
			inss = salarioNovo * 0.11;
			irpf = salarioNovo * 0.07;
			JOptionPane.showMessageDialog(null, "Folha de pagamento" + "\n" + "\nSalário bruto: " + df.format(salarioNovo) + "\nDesconto INSS: " + df.format(inss) + "\nDesconto IRPF: " + df.format(irpf) + "\nDesconto Plano de saúde: " + planoSaude + "\nSalário líquido: " + df.format(salarioNovo - inss - irpf - planoSaude));
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Algo deu errado!");
		}
	}
}
