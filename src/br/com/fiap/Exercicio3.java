package br.com.fiap;

import javax.swing.JOptionPane;

public class Exercicio3 {
	public static void main(String[] args) {
		float baseMaior, baseMenor, altura, resultado;
		
		try {
			baseMaior = Float.parseFloat(JOptionPane.showInputDialog("Digite a base maior: "));
			baseMenor = Float.parseFloat(JOptionPane.showInputDialog("Digite a base menor: "));
			altura = Float.parseFloat(JOptionPane.showInputDialog("Digite a altura: "));
			resultado = ((baseMaior + baseMenor) * altura) / 2;
			JOptionPane.showMessageDialog(null, "O resultado é: " + resultado);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Formato incorreto!");
		}
	}
}
