package br.com.fiap;

import javax.swing.JOptionPane;

public class Exercicio2 {

	public static void main(String[] args) {
		float base, altura, area;
		
		try {
			base = Float.parseFloat(JOptionPane.showInputDialog("Digite a base: "));
			altura = Float.parseFloat(JOptionPane.showInputDialog("Digite a altura: "));
			area = (base * altura) / 2;
			JOptionPane.showMessageDialog(null, "A área é: " + area);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Formato incorreto!");
		}
	}
	
}
