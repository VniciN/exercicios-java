package br.com.fiap;

import javax.swing.JOptionPane;

public class Exercicio1 {

	public static void main(String[] args) {
		float base, altura, resultado;
		
		try {
			base = Float.parseFloat(JOptionPane.showInputDialog("Digite a base: "));
			altura = Float.parseFloat(JOptionPane.showInputDialog("Digite a altura: "));
			resultado = base * altura;
			JOptionPane.showMessageDialog(null, "O resultado é: " + resultado);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Formato incorreto.");
		}
	}
	
}
